<?php

namespace App\Providers;

use App\ConwayLife\Cell;
use App\ConwayLife\CellInterface;
use App\ConwayLife\Generation;
use App\ConwayLife\GenerationInterface;
use App\ConwayLife\Neighborhood;
use App\ConwayLife\NeighborhoodInterface;
use App\ConwayLife\Reaper;
use App\ConwayLife\ReaperInterface;
use Illuminate\Support\ServiceProvider;

class ConwayLife extends ServiceProvider
{

    public $bindings = [
        CellInterface::class => Cell::class,
        NeighborhoodInterface::class => Neighborhood::class,
        ReaperInterface::class => Reaper::class,
        GenerationInterface::class => Generation::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
