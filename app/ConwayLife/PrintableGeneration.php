<?php


namespace App\ConwayLife;

/**
 * Class PrintableGeneration
 * @package App\ConwayLife
 *
 * This decorator will only decorate the $this->getMessage() and $this->output()
 * methods in order to allow for a nice printable output on the bash shell
 */
class PrintableGeneration
implements GenerationInterface
{

    protected $generation;
    protected $config;


    public function __construct(GenerationInterface $gen)
    {
        $this->generation = $gen;

        $this->config = config(
            'conwayLife.printableGeneration',
            [' ',  'O'] // [0] matches 0 vitality, [1] matches alive cells
        );
    }

    /**
     * @inheritDoc
     */
    public function setMessage($msg): void
    {
        $this->generation->setMessage($msg);
    }

    /**
     * @inheritDoc
     */
    public function getMessage()
    {
        $msg = $this->generation->getMessage();
        if(is_array($msg))
        {
            $textArray = [];
            foreach ($msg as $key => $value)
            {
                $textArray[] = "$key: $value";
            }

            return implode(' - ', $textArray);
        }

        return (string)$msg;
    }

    /**
     * @inheritDoc
     */
    public function setDishSize(array $size): void
    {
        $this->generation->setDishSize($size);
    }

    /**
     * @inheritDoc
     */
    public function getDishSize(): array
    {
        return $this->generation->getDishSize();
    }

    /**
     * @inheritDoc
     */
    public function setGeneration(array $gen): void
    {
        $this->generation->setGeneration($gen);
    }

    /**
     * @inheritDoc
     */
    public function getGeneration(): array
    {
        return $this->generation->getGeneration();
    }

    /**
     * @inheritDoc
     */
    public function output()
    {
        [$dimX, $dimY] = $this->getDishSize();
        $colony = $this->getGeneration();

        system('clear');
        echo $this->getMessage() . "\n";
        echo '+' . str_repeat('-', $dimX) . '+' . "\n"; // top frame

        foreach ($colony as $y => $row)
        {
            echo '|';
            foreach ($row as $x => $cell)
            {
                echo $this->config[$cell];
            }
            echo "|\n";
        }
        echo '+' . str_repeat('-', $dimX) . '+' . "\n"; // bottom frame
    }
}
