<?php


namespace App\ConwayLife;

/**
 * Interface GenerationInterface
 * @package App\ConwayLife
 *
 * GenerationInterface objects acts as simplified snapshots of the colony at a given time (i.e. the generation)
 * This adds some redundancy but protects the inner colony from accidental modifications.
 * It also allow the decorators of this interface to ignore the implementation details of the Cell objects
 */
interface GenerationInterface
{
    /**
     * Sets a message that may accompany the given generation when displayed
     *
     * @param mixed $msg normally it will be a string
     */
    public function setMessage($msg): void;

    /**
     * The message that was set
     *
     * @return mixed|string
     */
    public function getMessage();

    /**
     * Sets the size of the canvas / petri dish that generated this generation
     *
     * @param array $size in the form ['x' => x, 'y' => y] represents the size of the petri dish
     */
    public function setDishSize(array $size): void;

    /**
     * Gets the size of the generation
     *
     * @return array in the form ['x' => x, 'y' => y]
     */
    public function getDishSize(): array;

    /**
     * Takes in an array of cells or of their values
     * It may perform a simplification of the original array before storing it
     *
     * @param array $gen
     */
    public function setGeneration(array $gen): void;

    /**
     * Returns the given array either in its original form or in whatever form
     * it has been stored inside the generation object
     *
     * @return array
     */
    public function getGeneration(): array;

    /**
     * A snapshot of the generation is returned in whatever form that may be useful for its representation
     * At its basic, this method will return the same as getGeneration() but not necessarily so
     *
     * @return mixed
     */
    public function output();
}
