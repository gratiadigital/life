<?php


namespace App\ConwayLife;

/**
 * Interface ReaperInterface
 * @package App\ConwayLife
 *
 * The Reaper decides the Fate of cells: can summon() them to life or kill() them or just let them be.
 * Notice that the Reaper can't see the whole colony that is encapsulated inside the PetriDish object.
 * Neither the Reaper decide which neighboring cells should be evaluated to determine Fates.
 * That is the job of a NeighborhoodInterface object.
 * The Reaper is a blind judge
 */
interface ReaperInterface
{
    /**
     * Reaping (or passing judgment on) cells is the main method of this interface
     * and it decides the destiny of the given cell. in fact,
     * this method will then call $cell->summon() or $cell->kill() or none of the two
     * based on the specific rules implemented by this Reaper.
     *
     * @param CellInterface $cell
     * @return bool true if status of the cell has been changed, false otherwise
     */
    public function reap(CellInterface $cell): bool;

    /**
     * A Reaper object may be associated to a specific generator of neighborhood offsets
     * which fit the specific rules of the Reaper
     *
     * @param NeighborhoodInterface $gen
     */
    public function setNeighborhoodGenerator(NeighborhoodInterface $gen);

    /**
     * This method may be call by the PetriDish object to retrieve
     * the most appropriate offset generator for this Reaper
     *
     * @return NeighborhoodInterface
     */
    public function getNeighborhoodGenerator(): NeighborhoodInterface;


    /**
     * This method should do any necessary calculation necessary to evaluate the neighborhood
     * (usually it's the sum of neighboring cells but could be anything else) and return
     * the result of such calculation so thet the PetriDish object can store the result
     * in the central Cell
     *
     * @param array $neighborhood
     * @return mixed
     */
    public function evalNeighborhood(array $neighborhood);
}
