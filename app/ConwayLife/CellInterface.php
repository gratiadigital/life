<?php


namespace App\ConwayLife;

/**
 * Interface CellInterface
 * @package App\ConwayLife
 *
 * Each element in the colony array should implement this interface.
 * The interface is flexible by design so that the cells could be different
 * than just a series of 0s and 1s
 */
interface CellInterface
{
    /**
     * This method may be called in order to make the cell be alive
     *
     */
    public function summon(): void;

    /**
     * This method may be called to kill the cell
     */
    public function kill(): void;

    /**
     * This method may be called to get the value stored in the cell.
     * Typically that will be either 0 or 1 but really it could be any sort of value
     *
     * @return mixed
     */
    public function getVitality();

    /**
     * This method mat be called to directly set the vitality value of the cell
     * hence it is implementation dependent
     *
     * @param mixed $vitality
     */
    public function setVitality($vitality): void;

    /**
     * Checks whether the cell is alive/on or not
     *
     * @return bool true if the cell is alive
     */
    public function isAlive(): bool;

    /**
     * Checks whether the cell is dead/off or not
     *
     * @return bool true if the cell is dead
     */
    public function isDead(): bool;

    /**
     * Sets a value that usually is simply the count of adjacent alive cells
     * yet could be anything else
     *
     * @param mixed $neighborhood usually the number of adjacent alive cells
     */
    public function setNeighborhood($neighborhood): void;

    /**
     * Returns a value that can be used for enforcing loneliness / overpopulation rules
     *
     * @return mixed usually the number of adjacent alive cells
     */
    public function getNeighborhood();

}
