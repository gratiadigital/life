<?php


namespace App\ConwayLife;

/**
 * Class Cell
 * @package App\ConwayLife
 *
 * This very simple implementation of the interface reflects the binary nature of the cells
 * in Conway's original design
 */
class Cell
implements CellInterface
{
    protected $vitality = 0;
    protected $neighborsCount = null; // it's very important that this defaults exactly to null to delay killing at gen 1

    const ALIVE_VALUE = 1;
    const DEAD_VALUE = 0;

    public function summon(): void
    {
        $this->setVitality(static::ALIVE_VALUE);
    }

    public function kill(): void
    {
        $this->setVitality(static::DEAD_VALUE);
    }

    public function getVitality()
    {
        return $this->vitality;
    }

    public function setVitality($vitality): void
    {
        $this->vitality = $vitality;
    }

    public function isAlive(): bool
    {
        return $this->vitality == static::ALIVE_VALUE;
    }

    public function isDead(): bool
    {
        return $this->vitality == static::DEAD_VALUE;
    }

    public function setNeighborhood($neighborhood): void
    {
        $this->neighborsCount = $neighborhood;
    }

    public function getNeighborhood()
    {
        return $this->neighborsCount;
    }
}
