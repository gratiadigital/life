<?php


namespace App\ConwayLife;

/**
 * Class Generation
 * @package App\ConwayLife
 *
 * This is the most basic implementation of the interface where the output is a simple bi-dimensional array
 * Each element of the array is the vitality value of its correspondent cell in the colony.
 * We could have chosen to put there a simple 0/1 or dead/alive value there,
 * in order to even better shadow the details of implentation of the Cells in the colony,
 * but we decided to store their actual vitality values for more flexibility
 */
class Generation
implements GenerationInterface
{
    protected $colony;
    protected $message;
    protected $xSize;
    protected $ySize;

    /**
     *
     * @param mixed $msg
     */
    public function setMessage($msg): void
    {
        $this->message = $msg;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setDishSize(array $size): void
    {
        [$x, $y] = $size;
        $this->xSize = $x;
        $this->ySize = $y;
    }

    public function getDishSize(): array
    {
        return [$this->xSize, $this->ySize];
    }

    public function setGeneration(array $gen): void
    {
        $this->colony = $gen;
    }

    public function getGeneration(): array
    {
        return $this->colony;
    }

    public function output()
    {
        return $this->getGeneration();
    }
}
