<?php


namespace App\ConwayLife;


use Illuminate\Support\Facades\App;

/**
 * Class PetriDish
 * @package App\ConwayLife
 *
 * This, as the petri dish that contains your colony of cells, is the main class in the package.
 * It cannot be instantiated directly. Use the two provided factory methods instead:
 * @see static::factorRandomly()
 * @see static::factorByLoading()
 *
 * Use either the Generator $this->generate() or the method $this->generateOnce() to make your colony develop.
 *
 * Please note that this class uses several injected dependencies to rule the life of its cells.
 * Such dependencies are bound to their interfaces by the Service Container and then injected
 * (CellInterface, ReaperInterface, NeighborhoodInterface, GenerationInterface)
 * Some method allows you to override the defaults and use different implementations of those interfaces.
 * To change the defaults themselves, change the bingings in the ConwayLife Service Provider
 */
class PetriDish
{
    const CONFIG_FILE = 'conwayLife.petriDish';
    const MAX_RAND_DENSITY = 10;

    protected $colony = [];
    protected $defaultReaper;
    protected $defaultNeighborhoodGenerator;

    protected $generation = 0;
    protected $maxGenerations;

    protected $x;
    protected $y;

    protected $config;

    /**
     * @return ReaperInterface
     */
    public function getDefaultReaper(): ReaperInterface
    {
        return $this->defaultReaper;
    }

    /**
     * @param ReaperInterface $defaultReaper
     */
    public function setDefaultReaper(ReaperInterface $defaultReaper): void
    {
        $this->defaultReaper = $defaultReaper;
        $this->setDefaultNeighborhoodGenerator($defaultReaper->getNeighborhoodGenerator());
    }

    /**
     * @return NeighborhoodInterface
     */
    public function getDefaultNeighborhoodGenerator(): NeighborhoodInterface
    {
        return $this->defaultNeighborhoodGenerator;
    }

    /**
     * @param NeighborhoodInterface $defaultNeighborhoodGenerator
     */
    public function setDefaultNeighborhoodGenerator(NeighborhoodInterface $defaultNeighborhoodGenerator): void
    {
        $this->defaultNeighborhoodGenerator = $defaultNeighborhoodGenerator;
        $this->defaultNeighborhoodGenerator->setDishSize($this->getX(),$this->getY());
    }


    /**
     * PetriDish constructor is protected
     * Use factory methods instead
     *
     * @param ReaperInterface $reaper   a ReaperInterface object (@see App\Providers\ConwayLife.php)
     * @param int|null $x       X dimsnsion (columns)
     * @param int|null $y       y dimension (rows)
     * @param int|null $gens    requested number of iterations
     * @throws \Exception
     */
    protected function __construct(ReaperInterface $reaper, int $x = null, int $y = null, int $gens = null)
    {
        $c = $this->config = config(
            static::CONFIG_FILE,
            [
                'defaultX' => 10,
                'defaultY' => 10,
                'maxX' => 50,
                'maxY' => 50,
                'maxGenerations' => 10,
            ]
        );

        //
        // basic data validation
        //
        $this->setX($x ?? $c['defaultX'])->setY($y ?? $c['defaultY']);
        $this->maxGenerations = $gens ?? $c['maxGenerations'];
        if(0 > $this->getX() || $this->getX() > $c['maxX'])
        {
            throw new \Exception("X dimension $x is out of scale");
        }
        if(0 > $this->getY() || $this->getY() > $c['maxY'])
        {
            throw new \Exception("Y dimension $y is out of scale");
        }
        if(0 > $this->maxGenerations || $this->maxGenerations > $c['maxGenerations'])
        {
            throw new \Exception("Requested number of generations $gens  is out of scale");
        }

        //
        // stores the main injected dependencies
        // (setting the default reaper will in turn set the default Neighborhood generator)
        //
        $this->setDefaultReaper($reaper);
    }


    /**
     * This method is called by the factory methods in order to populate the colony in the dish
     * with actual cells.
     *
     * @param int $x    X position in the grid
     * @param int $y    y position in the grid
     * @param CellInterface $newCell    a CellInterface object (@see App\Providers\ConwayLife.php)
     */
    protected function addToColony(int $x, int $y, CellInterface $newCell)
    {
        $this->colony[$y][$x] = $newCell;
    }


    /**
     * This method can be used to factor a PetriDish containing a randomly generated colony
     * of given dimensions and given population density
     *
     * @param int $x            dimension (columns)
     * @param int $y            dimension (rows)
     * @param int $density      a number between 1 and static::MAX_RAND_DENSITY (the higher the number the more alive cells)
     * @param int|null $gens    requested number of generations
     * @return PetriDish        the PetriDish object containing the colony
     * @throws \Exception
     */
    public static function factorRandomly(int $x, int $y, int $density = 5, int $gens = null): PetriDish
    {
        $dish = new static(App::make(ReaperInterface::class), $x, $y, $gens);

        //
        // Y has been put in the outer loop to better mimic the way a multidimensional array would be written
        //
        for($row = 0; $row < $dish->getY(); $row++)
        {
            for($col = 0; $col < $dish->getX(); $col++)
            {
                $newCell = App::make(CellInterface::class);
                if(mt_rand(1, static::MAX_RAND_DENSITY) <= $density)
                {
                    $newCell->summon();
                }
                else
                {
                    //
                    // redundant but safe
                    //
                    $newCell->kill();
                }
                $dish->addToColony($col, $row, $newCell);
            }
        }

        return $dish;
    }

    /**
     * This factory method may be used to retrieve a PetriDish containing a very specific pattern of cells.
     * This method can be very useful to process very well known, interesting patterns as well as for testing
     *
     * @param array $cells      a bi-dimensional array of values representing the wanted grid of cells
     * @param string $mode      in 'lose' mode the values of the array will only be considered in terms of:
     *                          bool true = alive / bool false = dead; in any other mode, the actual values
     *                          of the array will be injected inside the cells
     * @param int|null $gens    requested number of generations
     * @return PetriDish
     * @throws \Exception
     */
    public static function factorByLoading(array $cells, string $mode = 'lose', int $gens = null): PetriDish
    {
        //
        // size of the grid is determined by the size of the passed array
        //
        $y = count($cells); // rows
        $x = count($cells[0]); // columns

        $dish = new static(App::make(ReaperInterface::class), $x, $y, $gens);

        //
        // Y has been put in the outer loop to better mimic the way a multidimensional array would be written
        //
        for($row = 0; $row < $dish->getY(); $row++)
        {
            for($col = 0; $col < $dish->getX(); $col++)
            {
                $newCell = App::make(CellInterface::class);
                if($mode == 'lose' && (bool)$cells[$row][$col] )
                {
                    //
                    // in 'lose' mode we only check whether suggested values are true or false
                    // and we rely on the implemented CellInterface class to decide what vitality values
                    // should be set
                    //
                    $newCell->summon();
                }
                elseif($mode == 'lose')
                {
                    $newCell->kill();
                }
                else
                {
                    //
                    // if the mode is not 'lose' we will implant in the Cell object the exact value
                    // that is presented by the loaded array
                    //
                    $newCell->setVitality($cells[$row][$col]);
                }

                $dish->addToColony($col, $row, $newCell);
            }
        }

        return $dish;
    }





    /**
     * @return int|mixed
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param int|mixed $x
     * @return PetriDish
     */
    public function setX($x)
    {
        //
        // prevents from accidentaly changing the size of the grid after initial setup is done
        //
        if(!empty($this->colony))
        {
            throw new \Exception('X dimension of the colony cannot be changed');
        }

        $this->x = $x;
        return $this;
    }

    /**
     * @return int|mixed
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param int|mixed $y
     * @return PetriDish
     */
    public function setY($y)
    {
        //
        // prevents from accidentaly changing the size of the grid after initial setup is done
        //
        if(!empty($this->colony))
        {
            throw new \Exception('Y dimension of the colony cannot be changed');
        }


        $this->y = $y;
        return $this;
    }


    /**
     * This Generator will produce generation after generation of cells,
     * untill the set max number of generations is reached
     *
     * @return \Generator the generator yields object that implement the GenerationInterface
     */
    public function generate()
    {
        while($this->generation < $this->maxGenerations)
        {
            yield $this->generateOnce();
        }
    }

    /**
     * This method implements the core of the generation process.
     * Cells will be sent to the Reaper injected object to decide of their destiny
     * (based on evaluation of their neighborhood done during the previous generation)
     * Cells' neighoborhoods will be then (re)evaluated (births and deaths will depend on this)
     * Finally, a GenerationInterface object will returned, which is like a snapshot of the colony at this time.
     *
     * @param ReaperInterface|null $r   you can optionally use a different Reaper for this one generation
     *                                  if you call this method directly.
     * @return GenerationInterface
     */
    public function generateOnce(ReaperInterface $r = null): GenerationInterface
    {
        //
        // update counter
        //
        $this->generation++;
        $startTime = time();

        //
        // loads relevant data and setup
        //
        $colony = $this->colony;
        $reaper = $this->getDefaultReaper();
        $offsetsGenerator = $this->getDefaultNeighborhoodGenerator();

        //
        // you may use different sets of rules for each generation
        //
        if(!is_null($r))
        {
            $reaper = $r;
            $offsetsGenerator = $r->getNeighborhoodGenerator();
            $offsetsGenerator->setDishSize($this->getX(), $this->getY());
        }

        //
        // births and deaths from the previous generation's counting
        //
        foreach($colony as $y => $row)
        {
            foreach ($row as $x => $cell)
            {
                $reaper->reap($cell);
            }
        }

        //
        // calculates neighborhoods
        //
        foreach($colony as $y => $row)
        {
            foreach ($row as $x => $cell)
            {
                //
                // gather pointers to the neighboring cells in an array
                //
                $neighbors = [];
                foreach ($offsetsGenerator->generateOffsets($x, $y) as $offset)
                {
                    [$neighborX, $neighborY] = $offset;
                    $neighbors[] = $this->colony[$neighborY][$neighborX];
                }

                //
                // The Reaper object evaluates and return the accounting for the neighborhood
                // which is stored inside the central cell for reaping at the next generation
                //
                $cell->setNeighborhood(
                    $reaper->evalNeighborhood($neighbors)
                );
            }
        }


        //
        // returns a GenerationInterface object as a snapshot of the present situation
        //
        $genObject = App::make(GenerationInterface::class);
        $genObject->setDishSize([$this->getX(), $this->getY()]);
        $genObject->setMessage(
            [
                'grid size' => "X{$this->getX()}, Y{$this->getY()}",
                'generation' => $this->generation,
                'time' => time() - $startTime,
            ]
        );

        $mirror = [];
        foreach($colony as $y => $row)
        {
            foreach ($row as $x => $cell)
            {
                $mirror[$y][$x] = $cell->getVitality();
            }
        }
        $genObject->setGeneration($mirror);

        return $genObject;

    }
}
