<?php


namespace App\ConwayLife;

/**
 * Interface NeighborhoodInterface
 * @package App\ConwayLife
 *
 * Given a petri dish size (i.e. given the size of your colony grid)
 * and given the position of a cell being evaluated inside that grid,
 * objects of this interface will generate a series of x,y coordinates
 * for each and every cell that is considered to be neighboring the given one
 */
interface NeighborhoodInterface
{
    /**
     * Stores the dimensions of the petri dish (i.e. of the array of the cells)
     * which are necessary to calculate position offsets for neighboring cells in the array
     *
     * @param int $x    columns
     * @param int $y    rows
     */
    public function setDishSize(int $x, int $y): void;

    /**
     * Generator of offsets
     * Depending on the implementation of the PetriDish internal array, these offsets
     * may be an array of single values or array of two x,y offsets
     *
     * @param int $x   X position of the central cell
     * @param int $y   Y position of the central cell
     * @return mixed|array   of calculated positions in the grid (one per each neighboring cell)
     */
    public function generateOffsets(int $x, int $y);
}
