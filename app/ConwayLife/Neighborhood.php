<?php


namespace App\ConwayLife;

/**
 * Class Neighborhood
 * @package App\ConwayLife
 *
 * This Neighorhood class will generate the x,y position of the classic 8 cells
 * that surround a given cell all around. (it's the classic Conway setup)
 *
 * Please note: by default, this class will consider the first row as neighboring the last one (and viceversa),
 * and the first column as neighboring the last one (and viceversa), therefore simulating a spherical/infinite grid.
 * This way ALL cells have 8 neighbors. You can turn this behaviour off by acting on the config array.
 * If you enforce borders, cells on teh edge will have less than 8 neighbors and will be more prone to die.
 */
class Neighborhood
implements NeighborhoodInterface
{
    protected $xSize = 0;
    protected $ySize = 0;

    protected $config;


    public function __construct(int $x = 0, int $y = 0)
    {

        $this->xSize = $x;
        $this->ySize = $y;


        //
        // reads the definition of neighboring cells from the config
        //
        $c = $this->config = config('conwayLife.neighborhood', []);

        if(empty($c))
        {
            throw new \Exception('Missing neighborhood definition');
        }
    }

    /**
     * @inheritDoc
     * @param int $x
     * @param int $y
     * @throws \Exception
     */
    public function setDishSize(int $x, int $y): void
    {
        if($x < 0 || $y < 0)
        {
            throw new \Exception('Invalid parameter for grid size');
        }

        $this->xSize = $x;
        $this->ySize = $y;
    }

    /**
     * Overrides the correspondent config setting
     * (mostly useful for testing)
     *
     * @param bool $enforce
     */
    public function enforceBorders(bool $enforce)
    {
        $this->config['borders'] = $enforce;
    }


    /**
     * @inheritDoc
     * @param int $x    X position in the grid of the central cell
     * @param int $y    Y position in the grid of the central cell
     * @return array    Each array contains a couple of absolute coordinates [x, y]
     */
    public function generateOffsets(int $x, int $y)
    {
        $offsets = $this->config['offsets'];
        $borders = $this->config['borders'];

        //
        // calculates an absolute position from each offset
        //
        foreach ($offsets as $offset)
        {
            $neighborX = $x + $offset['x'];
            if($neighborX < 0 && $borders)
            {
                //
                // if borders are enforced, cells on the edges have less neighbors than the central cells
                //
                continue;
            }
            elseif($neighborX >= $this->xSize && $borders)
            {
                //
                // if borders are enforced, cells on the edges have less neighbors than the central cells
                //
                continue;
            }
            elseif($neighborX < 0)
            {
               $neighborX += $this->xSize; // this neighbor is on the other side of the grid
            }
            elseif($neighborX >= $this->xSize)
            {
               $neighborX -= $this->xSize;
            }


            $neighborY = $y + $offset['y'];
            if($neighborY < 0 && $borders)
            {
                //
                // if borders are enforced, cells on the edges have less neighbors than the central cells
                //
                continue;
            }
            elseif($neighborY >= $this->ySize && $borders)
            {
                //
                // if borders are enforced, cells on the edges have less neighbors than the central cells
                //
                continue;
            }
            elseif($neighborY < 0)
            {
                $neighborY += $this->ySize; // this neighbor is on the other side of the grid
            }
            elseif($neighborY >= $this->ySize)
            {
                $neighborY -= $this->ySize;
            }

            yield [$neighborX, $neighborY];
        }

    }
}
