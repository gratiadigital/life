<?php


namespace App\ConwayLife;

/**
 * Class Reaper
 * @package App\ConwayLife
 *
 * This Reaper implements the classic Fate rules designed by Conway for his original game of life.
 * Therefore a NeighborhoodInterface object that implements the usual "ring" of 8 neighboring cells
 * should be injected if you want to obtain the expected results
 */
class Reaper
implements ReaperInterface
{
    protected $neighborhoodGeneratorGenerator;

    /**
     * Reaper constructor.
     * @param NeighborhoodInterface $gen the object that generates the addresses of the cells neighboring the one being judged
     */
    public function __construct(NeighborhoodInterface $gen)
    {
        $this->setNeighborhoodGenerator($gen);
    }


    /**
     * @inheritDoc
     * @param CellInterface $cell
     * @return bool
     */
    public function reap(CellInterface $cell): bool
    {
        $neighborhood = $cell->getNeighborhood();
        if(is_null($neighborhood))
        {
            //
            // on the first generation there will be no births and no deaths
            //
            return false;
        }

        $alive = $cell->isAlive();
        switch(true)
        {
            //
            // all cases in which the status of the cell needs to change
            //
            case $alive && $neighborhood < 2:
            case $alive && $neighborhood > 3:
            case !$alive && $neighborhood == 3:
                if($alive)
                {
                    $cell->kill();
                }
                else
                {
                    $cell->summon();
                }
                return true;
                break;

            default:
                return false;

        }
    }

    public function setNeighborhoodGenerator(NeighborhoodInterface $gen)
    {
        $this->neighborhoodGeneratorGenerator = $gen;
    }

    public function getNeighborhoodGenerator(): NeighborhoodInterface
    {
        return $this->neighborhoodGeneratorGenerator;
    }

    /**
     * @inheritDoc
     * @param array $neighborhood
     * @return int|mixed
     */
    public function evalNeighborhood(array $neighborhood)
    {
        $evaluation = 0;

        foreach ($neighborhood as $cell)
        {
            $evaluation += $cell->getVitality();
        }

        return $evaluation;
    }
}
