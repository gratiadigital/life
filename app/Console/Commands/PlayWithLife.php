<?php

namespace App\Console\Commands;

use App\ConwayLife\PetriDish;
use App\ConwayLife\PrintableGeneration;
use Illuminate\Console\Command;

class PlayWithLife
extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'life:rand {x} {y} {density=5} {generations=10}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a random cells colony and play!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $x = $this->argument('x');
        $y = $this->argument('y');
        $density = $this->argument('density');
        $gens = $this->argument('generations');

        $petriDish = PetriDish::factorRandomly($x, $y,$density, $gens);
        foreach ($petriDish->generate() as $generation)
        {
            $printable = new PrintableGeneration($generation);
            $printable->output();
            unset($printable);
            sleep(1);
        }

    }
}
