#Conway's Game of Life
This is a very OOP oriented version of the famous mathematic game, for the [Laravel framework](https:laravel.com)

## Getting started
This implementation has NOT been packaged for distribution via composer as a standalone module, sorry!

This means that you will have to pull the whole Laravel application. Once you have deployed it locally, you will need to run: `composer install`in order to pull down all vendors' dependencies and the Laravel framework itself.

More info about how to install Laravel can be found [here](https://laravel.com/docs/8.x/installation). The whole process shouldn't take more than a few minutes.

This Life game was developed and tested on Laravel 8.0 on a [Homestead](https://laravel.com/docs/8.x/homestead) Ubuntu virtual machine running PHP7.3 (No DB or webserver is required)

## How to play (for beginners)

Nothing's easier!<br>
This implementation includes an [artisan command](https://laravel.com/docs/8.x/artisan#writing-commands) that you can easily use to randomly create a colony and see it develop for a chosen amount of generations on your shell.

Example:<br>
`php artisan life:rand 10 10 5 15`

The command above will:
 - create a 10 by 10 grid of cells (x,y dimensions)
 - with a population density of 5 (out of 10)
 - and will run the game for 15 generations<br>
 
(density and generations are optional parameters which default to 5 and 10 respectively and you may omit)

## How to play (for hardcore players)

This implementation, however, allows you to do much much more than just that!

### How cells colonies are created

The above mentioned artisan command is based on this factory method:<br>
`App\ConwayLife\PetriDish::factorRandomly()`<br>
But a different factory method can be used to shape the colony over any of the most [famous patterns](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) :<br>
`App\ConwayLife\PetriDish::factorByLoading()`<br>
This latter method takes a bi-dimensional array as the pattern for your colony. You can play with either of these methods inside the [Tinker](https://laravel.com/docs/8.x/artisan#tinker) shell or within your own code.<br>
(please note that you cannot instantiate a PetriDish object directly)

### Rules of Life

This implementation comes to you with the usual rules by Conway. However, this may be changed.

Class `App\ConwayLife\Reaper` is injected inside the `PetriDish` class and carries all the rules that regulate the life of cells.

Class `App\ConwayLife\Neighborhood` is injected inside the `Reaper` class (and, from there, into `PetriDish`) and carries all the rules that determine which cells are to be considered as "neighboring". At present it's the usual 8 cells all around each given cell. But this could be changed. You could use different neighboring rules that match your different life and death rules.

For instance, your cells (`App\ConwayLife\Cell`) might contain colors or names of flowers, instead of 0 and 1. The neighboring cells you care about might be only the ones on the diagonals. And births and deaths may be determined by how well two colors go together instead of by a simple count.

Also, by creating further decorators of the class `App\ConwayLife\Generation`, besides the `PrintableGeneration` class that is being used by the above mentioned artisan command `life:rand`, you may easily output your generations in different formats (for instance, you might easily create a JsonGeneration decorator to expose an API endpoint and show your colony on the web).

Finally, you can make the generations advance one step at a time with the method `App\ConwayLife\PetriDish::generateOnce()` Notice that this method allows you to override the default Reaper object, i.e. you could easily use different sets of rules for each different generation!

### Code structure

Inside the App/ConwayLife folder you will also find a number of interfaces:

 - ReaperInterface.php
 - NeighborhoodInterface.php
 - GenerationInterface.php
 - CellInterface.php

Those interfaces represent the backbone of this implementation: feel free to play with them!

Inside the App/Providers folder you will find `ConwayLife` class that binds all default implentation to the above mentioned interfaces. By changing these bindings, you may change which implementation shall be injected by default.

Some of the mentioned classes have config files inside config/conwayLife folder.

All classes and methods have been extensively commented. Check the comments for all implementation details. Some basic unit testing has been provided too (not really enough but the one that really mattered). Enjoy!

## Contributing

Conway, of course!

## Code of Conduct

Please don't be cruel to your cells, they don't mean no arms to you even if they squirrel around

## Security Vulnerabilities

Not much to say about this... it's just a simple employment test after all!

## License

This implementation was created by David Ballerini. If you like it, hire me!
