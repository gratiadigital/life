<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

/**
 * Class PetriDishTest
 * @package Tests\Feature
 *
 * NOT COMPLETE!!! THE IDEA IS TO TEST AGAINST THE GENERATED GENERATION
 * AT ITERATION 2
 */
class PetriDishTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }



    public function gridsProvider()
    {
        return [
            'single cell dies' => [
                'startingGrid' => [
                    [0,0,0],
                    [0,1,0],
                    [0,0,0],
                ],
                'mode' => 'lose',
                'gens' => 2,
                'endingGrid' => [
                    [0,0,0],
                    [0,0,0],
                    [0,0,0],
                ]
            ]
        ];
    }
}
