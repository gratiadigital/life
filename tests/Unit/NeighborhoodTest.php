<?php

namespace Tests\Unit;

use App\ConwayLife\Neighborhood;
use Tests\TestCase;

class NeighborhoodTest extends TestCase
{
    /**
     * A basic unit test example.
     * @dataProvider offsetDataProvider
     * @return void
     */
    public function testNeighborhoodGenerator($xSize, $ySize, $x, $y, $borders, $result)
    {
        $gen = new Neighborhood();
        $gen->setDishSize($xSize, $ySize);
        $gen->enforceBorders($borders);
        $generatedPositions = [];
        foreach ($gen->generateOffsets($x,$y) as $offset)
        {
            $generatedPositions[] = $offset;
        }

        $this->assertTrue($generatedPositions == $result);

    }

    public function offsetDataProvider()
    {
        return [
            'center with borders' => [
                'xSize' => 3,
                'ySize' => 3,
                'x' => 1,
                'y' => 1,
                'borders' => true,
                'result' => [
                    [0,0],
                    [1,0],
                    [2,0],
                    [0,1],
                    [2,1],
                    [0,2],
                    [1,2],
                    [2,2],
                ],
            ],
            'center without borders' => [
                'xSize' => 3,
                'ySize' => 3,
                'x' => 1,
                'y' => 1,
                'borders' => false,
                'result' => [
                    [0,0],
                    [1,0],
                    [2,0],
                    [0,1],
                    [2,1],
                    [0,2],
                    [1,2],
                    [2,2],
                ],
            ],
            'top left corner with borders' => [
                'xSize' => 3,
                'ySize' => 3,
                'x' => 0,
                'y' => 0,
                'borders' => true,
                'result' => [

                    [1,0],
                    [0,1],
                    [1,1],
                ],
            ],
            'top left corner without borders' => [
                'xSize' => 3,
                'ySize' => 3,
                'x' => 0,
                'y' => 0,
                'borders' => false,
                'result' => [
                    [2,2],
                    [0,2],
                    [1,2],
                    [2,0],
                    [1,0],
                    [2,1],
                    [0,1],
                    [1,1],
                ],
            ],
            'bottom right corner with borders' => [
                'xSize' => 3,
                'ySize' => 3,
                'x' => 2,
                'y' => 2,
                'borders' => true,
                'result' => [
                    [1,1],
                    [2,1],
                    [1,2],

                ],
            ],
            'bottom right corner without borders' => [
                'xSize' => 3,
                'ySize' => 3,
                'x' => 2,
                'y' => 2,
                'borders' => false,
                'result' => [
                    [1,1],
                    [2,1],
                    [0,1],
                    [1,2],
                    [0,2],
                    [1,0],
                    [2,0],
                    [0,0],
                ],
            ],
        ];
    }
}
