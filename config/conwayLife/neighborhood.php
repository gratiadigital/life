<?php

/**
 * This config array defines the usual Conway Life "neighborhood" of cells,
 * i.e. the 8 cells that directly surround any given cell in the grid.
 * Each of this neighboring cell is represented in terms of [x,y] offsets
 * from the surrounded cell
 */
return [
    'offsets' => [
        ['x' => -1, 'y' => -1],
        ['x' => 0, 'y' => -1],
        ['x' => 1, 'y' => -1],

        ['x' => -1, 'y' => 0],

        ['x' => 1, 'y' => 0],

        ['x' => -1, 'y' => 1],
        ['x' => 0, 'y' => 1],
        ['x' => 1, 'y' => 1],
    ],

    //
    // determines whether the grid is "infinite" that is:
    // whether the first and last row and the first and last columns
    // are neighboring or not
    //
    'borders' => false
];
