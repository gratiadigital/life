<?php

use App\ConwayLife\ReaperInterface;
use App\ConwayLife\NeighborhoodInterface;
use App\ConwayLife\GenerationInterface;
use App\ConwayLife\CellInterface;

return [
    'defaultX' => 10,
    'defaultY' => 10,
    'maxX' => 50,
    'maxY' => 50,
    'maxGenerations' => 10,
];
